#include <Arduino.h>

const int PWMfreq = 50; // 50Hz PWM
const int PWMres = 10; // 10-bit PWM
const int LeftFanPin = 25;
const int RightFanPin = 26;
const int LeftFanCtl = 0;
const int RightFanCtl = 1;

#define RXD2 16
#define TXD2 5

void writePercent(int channel, float percentage)
{
	// Convert percentage to PWM value and write it to a fan
	ledcWrite(channel, int(percentage / 100.f * (1 << PWMres)));
}

void writeSpeed(int channel, float percentage)
{
	// Clamp percentage
	if (percentage > 100) percentage = 100;
	if (percentage < 0) percentage = 0;

	// ESC speed control is in the range of 5% and 10% PWM
	// (at 50Hz, effective range is 1ms-2ms, with one pulse taking 20ms)
	// With some approximations, on our hovercraft taking 5.75% as the
	// minimum offers the best balance between the two direction fans.
	writePercent(channel, 5.75f + percentage / 23.53f);
}

// direction: -1 = left, +1 = right
// strength determines how fast the fans spin, [0, 1]
void writeDirection(float direction, float strength)
{
	// Clamp direction
	if (direction < -1.f) direction = -1.f;
	if (direction > 1.f) direction = 1.f;

	// Clamp strength
	if (strength < 0.f) strength = 0.f;
	if (strength > 1.f) strength = 1.f;

	// The hovercraft uses 2 fans placed at an angle opposite each other in order to
	// control direction. Controlling direction means setting the speed on the fans 
	// inversely proportional.
	// e.g. to go left, the left fan would be at 0% and the right fan would be at 100%
	// for going forward, both fans are at 50%
	writeSpeed(LeftFanCtl, strength * (50 + direction * 50));
	writeSpeed(RightFanCtl, .75f * strength * (50 - direction * 50));
}

void setup()
{
	// Set up physical <-> virtual pins
	ledcSetup(LeftFanCtl, PWMfreq, PWMres);
	ledcAttachPin(LeftFanPin, LeftFanCtl);

	ledcSetup(RightFanCtl, PWMfreq, PWMres);
	ledcAttachPin(RightFanPin, RightFanCtl);

	// Begin with no movement
	writeSpeed(LeftFanCtl, 0);
	writeSpeed(RightFanCtl, 0);
	delay(5000);

	Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
}

float direction = 0.0f;
float strength = 0.0f;

void loop()
{
	// Bluetooth controls for the hovercraft
	// The direction and speed are increased/decreased in steps
	while(Serial2.available())
	{
		char c = Serial2.read();
		switch(c)
		{
			case 'L':
				direction = (direction < -1.f ? -1.f : direction -0.1f);
				break;
			case 'R':
				direction = (direction > 1.f ? 1.f : direction +0.1f);
				break;
			case 'U':
				strength = (strength > .7f ? .7f : strength +0.1f);
				break;
			case 'D':
				strength = (strength < 0.f ? 0.f : strength -0.1f);
				break;
			case 'S':
				direction = strength = 0.f;
				break;
			case 'Q':
				direction = -1.f;
				strength = .7f;
				break;
			case 'E':
				direction = 1.f;
				strength = .7f;
				break;
		}
	}
	writeDirection(direction, strength);
}
